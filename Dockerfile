FROM alpine:latest
RUN apk --update add python3 py3-pip
COPY . /app/
WORKDIR /app/
ENV FLASK_APP /app/app.py
ENV FLASK_DEBUG True
CMD pip install -r /app/requirements.txt && flask run --host=0.0.0.0
