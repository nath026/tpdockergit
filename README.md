# tpDockerGit

## How to run the python application locally

- Clone the project 
- Install python version 3.8 from : https://www.python.org/downloads/
- Install flask and its dependencies with `pip install -r requirements.txt`
- Set up environment variables such as `set FLASK_APP=app.py`
- Run flask server with `flask run`
- Application should be running on `http://localhost:5000`

## How to run the dockerfile in the container
### Prerequisite
- Have Docker installed locally

### Steps
- Clone the project
- Create the docker image named 'flask_web_app' with `docker build -f Dockerfile -t flask_web_app .`
- Once the image is created, you should be able to list it using `docker images`
- To run the container, run `docker run -p 5000:5000 flask_web_app`
- The application should be available at `http://localhost:5000`
- otherwise use the docker-compose.yml
- the image will be created with docker-compose pull
- relaunch the container with docker-compose up
- The application should be available at  http://localhost:5000/

